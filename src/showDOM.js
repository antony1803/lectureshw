const showDOM = arg => {
    let tab = 1;
    console.log(arg.localName)
    function showMe(arg) {
        const currentTag = Array.prototype.slice.call(arg.children);
        currentTag.forEach(el => {
            console.log(' '.repeat(tab) + el.localName);
        if (el.children) {
            tab += 1;
            showMe(el);
        }
        tab -= 1;
    });
    }
    return showMe(arg);
};

showDOM(document.body);